import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './Containers/App';
import registerServiceWorker from './registerServiceWorker';
import ErrorBoundary from './hoc/ErrorBoundary/ErrorBoundary';

ReactDOM.render(<ErrorBoundary><App /></ErrorBoundary>, document.getElementById('root'));
registerServiceWorker();
