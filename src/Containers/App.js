import React, { Component } from 'react';
import './App.css';
import axios from '../axios';
import withLoader from '../hoc/withLoader';

class App extends Component {

  state = {
    posts: {}
  };

  componentDidMount () {
    axios.get('/posts.json')
        .then(resp => {
          this.setState({posts: resp.data});
        })
  }

  render() {
    const posts = Object.keys(this.state.posts);
    return (
          <div className="App">
              {posts.map((id, index) => {
                  const post = this.state.posts[id];
                  return (
                      <div className="post" key={index}>
                        <h4>{post.title}</h4>
                        <p>{post.body}</p>
                      </div>
                  )
              })}
          </div>
    );
  }
}

export default withLoader(App, axios);
