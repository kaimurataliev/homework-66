import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://lesson-64-lab.firebaseio.com/'
});

instance.interceptors.request.use((req) => {
    console.log('[Запрос отправлен]', req);
    return req;
});

instance.interceptors.response.use((res) => {
    console.log('[Пришел ответ с данными]', res);
    return res;
});


export default instance;