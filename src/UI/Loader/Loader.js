import React from 'react';
import Picture from '../../assets/preloader/45.gif';
import './Loader.css';

const Loader = () => {
    return (
        <div className="preloader">
            <img src={Picture} alt="preloader gif"/>
        </div>
    )
};

export default Loader;