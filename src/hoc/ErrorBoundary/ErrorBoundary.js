import React, { Component } from 'react';
import './ErrorBoundary.css';
import axios from '../../axios';

class ErrorBoundary extends Component {
    state = {
        hasError: false,
        errorMessage: ''
    };

    componentDidCatch (error) {
        this.setState({hasError: true, errorMessage: error});
        axios.post('/errors.json', {error: JSON.stringify(error.message)})
    };


    render() {
        if(this.state.hasError) {
            return <div className="error">Sorry, it seems like error happened</div>
        } else {
            return this.props.children;
        }
    }
}

export default ErrorBoundary;