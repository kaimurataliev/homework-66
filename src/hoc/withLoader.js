import React, { Component, Fragment } from 'react';
import Loader from '../UI/Loader/Loader';

const withLoader = (WrappedComponent, axios) => {
    return class withLoader extends Component {
        constructor (props) {
            super(props);

            this.state = {
                isLoading: false
            };

            axios.interceptors.request.use((req) => {
                this.setState({isLoading: true});
                return req;
            });

            axios.interceptors.response.use((res) => {
                this.setState({isLoading: false});
                return res;
            }, error => {
                this.setState({isLoading: false});
                return error;
            });
        }

        render () {
            const showLoader = this.state.isLoading;
            return (
                <Fragment>
                    {showLoader && (<Loader/>)}
                    <WrappedComponent {...this.props}/>
                </Fragment>
            )
        }
    }
};

export default withLoader;